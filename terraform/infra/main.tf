################# Region of Infra #################
provider "aws" {
  version                 = "~> 2.7"
  #shared_credentials_file = var.key_credentials_path
  region                  = var.infra_region
  #region  = "ap-south-1"
}

################## Creating VPC ####################
module "vpc" {
  source               = "../vpc"
  cidr_block           = var.vpc_cidr_block
  instance_tenancy     = var.instanceTenancy
  enable_dns_support   = var.dnsSupport
  enable_dns_hostnames = var.dnsHostNames
  vpc_Name_tag         = "${var.env}_${var.vpc_tag_name}"
}

#########Creating  Public Subnet ##############
module "bastion_host_pub_subnet_az_a" {
  source            = "../subnets"
  vpc_id            = module.vpc.id
  cidr_subnet       = var.cidr_pub_subnet_az_a
  availability_zone = var.availability_zone_a
  subnet_name       = "${var.env}_${var.pub_subnet_name_az_a}"
}

############Creating Jenkins Private Subnet############
module "jenkins_host_private_subnet_az_a" {
  source            = "../subnets"
  vpc_id            = module.vpc.id
  cidr_subnet       = var.cidr_bastion_subnet_az_a
  availability_zone = var.availability_zone_a
  subnet_name       = "${var.env}_${var.jenkins_subnet_name_az_a}"
  }

####Creating Airflow Private Subnet############
module "airflow_host_private_subnet_az_a" {
  source            = "../subnets"
  vpc_id            = module.vpc.id
  cidr_subnet       = var.cidr_airflow_subnet_az_a
  availability_zone = var.availability_zone_a
  subnet_name       = "${var.env}_${var.airflow_subnet_name_az_a}"
}

  ############INTERNET GATEWAY#########################
module "internet_gateway" {
 source   = "../igw"
 vpc_id   = module.vpc.id
 igw_name = "${var.env}_${var.one_igw}"

}

################ELASTIC IP###########################
module "el_ip"{
  source = "../elasticip"
}

#############NAT GATEWAY##############################

module "nat_gateway" {
 source = "../nat"
 eip = module.el_ip.id
 pub_subnet = module.bastion_host_pub_subnet_az_a.subnet_id

}
###############Public Route Table#####################

module "pub_route_table" {
 source = "../route"
 vpc_id = module.vpc.id
 cidr_route = var.route_cidr
 igw_route = module.internet_gateway.igw_id
 route_name = "${var.env}_${var.route_tag_name}"

}

###Bastion public Subnet Association

module "bastion_subnet_association_to_routetable" {
  source = "../subnetAssociation"
  subnetid = module.bastion_host_pub_subnet_az_a.subnet_id
  routetable_id = module.pub_route_table.id

}

###############jENKINS Private Route Table#####################

module "jenkins_route_table" {
 source = "../route"
 vpc_id = module.vpc.id
 cidr_route = var.route_cidr_jenkins
 igw_route = module.nat_gateway.id
 route_name = "${var.env}_${var.jenkins_route_tag_name}"

}

###Subnet Association

module "jenkins_subnet_association_to_routetable" {
  source = "../subnetAssociation"
  subnetid = module.jenkins_host_private_subnet_az_a.subnet_id
  routetable_id = module.jenkins_route_table.id
}

###############Airflow Private Route Table#####################

module "airflow_route_table" {
 source = "../route"
 vpc_id = module.vpc.id
 cidr_route = var.route_cidr_airflow
 igw_route = module.nat_gateway.id
 route_name = "${var.env}_${var.airflow_route_tag_name}"

}

###Subnet Association

module "airflow_subnet_association_to_routetable" {
  source = "../subnetAssociation"
  subnetid = module.airflow_host_private_subnet_az_a.subnet_id
  routetable_id = module.airflow_route_table.id
}

############Security Group of Bastion Server #############

module "bastion_sg" {
 source = "../securitygroup"
 security_group_name = var.security_groupname_bastion
 vpc_id = module.vpc.id
 from_ports = var.from_ports_bastion
 to_ports = var.to_ports_bastion
 cidrblocks = var.cidrblocks_bastion
 tag_name_value = var.tag_name_value_bastion
}

############Security Group of Jenkins Server #############

module "jenkins_sg" {
 source = "../securitygroup"
 security_group_name = var.security_groupname_jenkins
 vpc_id = module.vpc.id
 from_ports = var.from_ports_jenkins
 to_ports = var.to_ports_jenkins
 cidrblocks = var.cidrblocks_jenkins
 tag_name_value = var.tag_name_value_jenkins
}

############Security Group of Airflow Server #############

module "airflow_sg" {
 source = "../securitygroup"
 security_group_name = var.security_groupname_airflow
 vpc_id = module.vpc.id
 from_ports = var.from_ports_airflow
 to_ports = var.to_ports_airflow
 cidrblocks = var.cidrblocks_airflow
 tag_name_value = var.tag_name_value_airflow
}

#############KEY PAIR################

module "key_pair" {
  source = "../keypair"
  keyname = var.namekey
  publickey =  var.keypublic
}

##################Jenkins EC2########################
module "jenkins_ec2" {
 source = "../ec2"
  ami_ec2 = var.ec2_ami
  ec2_type = var.type_ec2
  subnetid = module.jenkins_host_private_subnet_az_a.subnet_id
  ec2_sg = [module.jenkins_sg.id]
  publicip = "false"
  private_ip = "192.168.2.5"
  keypair-name = module.key_pair.id
  ec2_tag = "jenkins"

}

##################Airflow EC2########################
module "airflow_ec2" {
 source = "../ec2"
  ami_ec2 = var.ec2_ami
  ec2_type = var.type_ec2_airflow
  subnetid = module.airflow_host_private_subnet_az_a.subnet_id
  ec2_sg = [module.airflow_sg.id]
  publicip = "false"
  private_ip = "192.168.3.5"
  keypair-name = module.key_pair.id
  ec2_tag = "airflow"

}

##################Bastion EC2########################
module "bastion_ec2" {
 source = "../ec2_bastion"
  ami_ec2 = var.ec2_ami
  ec2_type = var.type_ec2
  subnetid = module.bastion_host_pub_subnet_az_a.subnet_id
  ec2_sg = [module.bastion_sg.id]
  publicip = "true"
  private_ip = "192.168.1.5"
  keypair-name = module.key_pair.id
  ec2_tag = "bastion"

}
terraform {
  backend "s3" {
    bucket = "airflow-amii"
    key    = "terraform.tfstate"
    region = "ap-south-1"
  }
}