########## env name##############
variable "env" {
  description = "env to deploy into, should typically ab_dev/ab_qa/ab_prod"
  default     = "dev"
}

###### Variable for Region ##########
variable "infra_region" {
  description = "region for the infra"
  default     = "ap-south-1"
}

variable "key_credentials_path" {
  description = "key credentials path"
  default     = ""
}

###################### VPC vars #####################
# VPC CIDR block
variable "vpc_cidr_block" {
  description = "The CIDR block for the VPC"
  default     = "192.168.0.0/16"
}

# VPC Tenancy
variable "instanceTenancy" {
  default = "default"
}

# VPC dns support value
variable "dnsSupport" {
  default = true
}

# VPC dns host name value
variable "dnsHostNames" {
  default = true
}

###########Tag for VPC
variable "vpc_tag_name" {
  description = "Tag for VPC Name fields"
  default     = "vpc"
}

################################################## Subnets ####################################################################

###########variables public subnet########
#### CIDR Block
variable "cidr_pub_subnet_az_a" {
  description = "The CIDR block for the subnet."
  default     = "192.168.1.0/24"
}

#### Tag Name 
variable "pub_subnet_name_az_a" {
  description = "name of public subnets"
  default     = "pub_subnet_az_a"
}


###########variables JEnkins private subnet########
#### CIDR Block
variable "cidr_bastion_subnet_az_a" {
  description = "The CIDR block for the subnet."
  default     = "192.168.2.0/24"
}

#### Tag Name 
variable "jenkins_subnet_name_az_a" {
  description = "name of public subnets"
  default     = "bastion_subnet_az_a"
}

###########variables Airflow private subnet########
#### CIDR Block
variable "cidr_airflow_subnet_az_a" {
  description = "The CIDR block for the subnet."
  default     = "192.168.3.0/24"
}

#### Tag Name 
variable "airflow_subnet_name_az_a" {
  description = "name of public subnets"
  default     = "airflow_subnet_az_a"
}
####### Two Availability zone for all subnets#########3
variable "availability_zone_a" {
  description = "The AZ for the subnet"
  default     = "ap-south-1a"
}

variable "availability_zone_b" {
  description = "The AZ for the subnet"
  default     = "ap-south-1b"
}

#### IGW Tag Name 
variable "one_igw" {
  default     = "igw"
}

#####PUBLIC ROUTE TABLE#######
variable "route_cidr" {
  default = "0.0.0.0/0"
}

variable "route_tag_name" {
 default = "public-Route-table"
}

#####Jenkins PRIVATE ROUTE TABLE#######
variable "route_cidr_jenkins" {
  default = "0.0.0.0/0"
}

variable "jenkins_route_tag_name" {
 default = "jenkins-Route-table"
}

#####Airflow PRIVATE ROUTE TABLE#######
variable "route_cidr_airflow" {
  default = "0.0.0.0/0"
}

variable "airflow_route_tag_name" {
 default = "Airflow-Route-table"
}

########Bastion SG#######
variable "security_groupname_bastion" {
  default = "bastion_security_group"
}

variable "from_ports_bastion" {
  default = "8080,22,80"
}

variable "to_ports_bastion" {
  default = "8080,22,80"
}

variable "cidrblocks_bastion" {
  default = ["0.0.0.0/0"]
}

variable "tag_name_value_bastion" {
  default = "bastion_sg"
}

########Jenkins SG#######
variable "security_groupname_jenkins" {
  default = "jenkins_security_group"
}

variable "from_ports_jenkins" {
  default = "8080,22"
}

variable "to_ports_jenkins" {
  default = "8080,22"
}

variable "cidrblocks_jenkins" {
  default = ["192.168.0.0/16"]
}

variable "tag_name_value_jenkins" {
  default = "Jenkins_sg"
}

########Airflow SG#######
variable "security_groupname_airflow" {
  default = "airflow_security_group"
}

variable "from_ports_airflow" {
  default = "8080,22"
}

variable "to_ports_airflow" {
  default = "8080,22"
}

variable "cidrblocks_airflow" {
  default = ["192.168.0.0/16"]
}

variable "tag_name_value_airflow" {
  default = "Jen_tom_sg"
}

 ##############KEY PAIR ########

 variable "namekey" {
   default = "deployer_key"
 }

 variable "keypublic" {
   default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCaXRp5ouqgXNPwEkm9OHlahVQivY0/ew+oByVIdPW8WzZAJ6H+RaRdeCzdVOKYti7VOdP6iBkstvXXCK99YTchopHY7wzLfd8L2AhB07m0wIhdj07AZn5GeYtWKkvByKHkSrCtkBKoopPkeP/1lK+VkBYcQ71x62QXfsUM840jcxjhyA66d1v638ycA9e8nEqxcrF310hpO4fh7jFVeuKyB/P1Tm37inZWIDmKsBJJaKjBeTBO+Qnle7lkl9vZlOBh+WAILpAJQ/G4J12oCebYAQbgis0kMNP+sWWMf0lEn82PyYsqjoBvyk8nmYWrHJqnv4cSSw+mG/GCA62IyJolElxnHCESowrMuEJykzrOY+HZ9PHAMxcYcu3YOe+6FMzjZvB9EOBH157CqIy0LdUgDDv1K8fObniMxE+tMR0ER9JFnytsYBTTxqAE/9FP40S9Fvvu9g4IbeAnJ5Cf+X54F2Gv+SrRpZyUPxOGKXqEqMIMfKcSlLmFAwocvM71wewfzdT4M/mWgGCgxlHIG+ZEXqmWc3r34lufefQbzbU18hHiPyT4LRmGnEAym+uxeu37VoDn10WV507GfuNvxYlHsu4DDVLbSyOSm+c/8Tbl1WoZFbf5jtiER3IVbOjGsUjFkw2dA9/1y48q3Nhm/cIW/2bXyLxqbAfJ50mVnMFN5w=="
 }

###############EC2#############
variable "ec2_ami" {
  default = "ami-0b44050b2d893d5f7"
}

variable "type_ec2" {
  default = "t2.micro"
}
variable "type_ec2_airflow" {
  default = "t2.2xlarge"
}

variable "tag_ec2" {
  default = "jenkins"
}