resource "aws_instance" "bastion_ec2" {

 ami = var.ami_ec2
 instance_type = var.ec2_type

 subnet_id = var.subnetid

 vpc_security_group_ids = var.ec2_sg
 associate_public_ip_address = var.publicip
 key_name = var.keypair-name
 private_ip = var.private_ip
 
tags = {
 Name = var.ec2_tag
}
provisioner "file" {
    source      = "../ec2_bastion/ansiblefiles"
    destination = "/home/ubuntu"
  }
  provisioner "file" {
    source      = "/var/jenkins_home/key"
    destination = "/home/ubuntu"
  }

  provisioner "remote-exec" {
     inline = [
       "sudo apt-get update && sudo add-apt-repository ppa:deadsnakes/ppa -y && sudo apt-get install -y python",
       "sudo apt-get install software-properties-common -y",
       "sudo apt-get install ansible -y"
     ]
}
 provisioner "remote-exec" {
     inline = [ "cd /home/ubuntu/ansiblefiles/jenkins_playbook && sudo ansible-playbook -i inventory jenkins.yml"

     ]
 }
 
  provisioner "remote-exec" {
     inline = [ "cd /home/ubuntu/ansiblefiles/airflow_playbook && sudo ansible-playbook -i inventory Apache_airflow.yml --vault-password-file=/home/ubuntu/key/vaultpass"

     ]
 }
   provisioner "remote-exec" {
      inline = [
        "sudo  sysctl -w net.ipv4.ip_forward=1",
        "sudo iptables -A INPUT -p tcp --dport 8080 -j ACCEPT",
        "sudo iptables -t nat -A PREROUTING -p tcp --dport 8080 -j DNAT --to-destination 192.168.2.5:8080",
        "sudo iptables -t nat -A POSTROUTING -p tcp --dport 8080 -j MASQUERADE",
        "sudo iptables-save"
      ]
 }
 
 provisioner "remote-exec" {
      inline = [
        "sudo  sysctl -w net.ipv4.ip_forward=1",
        "sudo iptables -A INPUT -p tcp --dport 80 -j ACCEPT",
        "sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j DNAT --to-destination 192.168.3.5:8080",
        "sudo iptables -t nat -A POSTROUTING -p tcp --dport 80 -j MASQUERADE",
        "sudo iptables-save"
      ]
 }


connection {
type = "ssh"
user = "ubuntu"
private_key = "${file("/var/jenkins_home/key/amii.pem")}"
host = "${self.public_ip}"
}
}
